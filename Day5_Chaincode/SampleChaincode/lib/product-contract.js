'use strict';
const { Contract }=require('fabric-contract-api')
class SampleContract extends Contract{
    async createProduct(ctx,productID,name,qty){
        const asset={
            name,
            quantity:qty,
            Ownership:'In Factory'
        }
        const buffer=Buffer.from(JSON.stringify(asset))
        await ctx.stub.putState(productID,buffer)
    }
    async readProduct(ctx,productID){
        const buffer=await ctx.stub.getState(productID)
        const asset=JSON.parse(buffer.toString())
        return asset
    }
}
module.exports=SampleContract